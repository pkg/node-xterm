Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2017-2022, [The xterm.js authors](https://github.com/xtermjs/xterm.js/graphs/contributors) (MIT License)
 2017-2019, The xterm.js authors (https://github.com/xtermjs/xterm.js)
 2014-2017, SourceLair, Private Company ([www.sourcelair.com](https://www.sourcelair.com/home)) (MIT License)
 2014-2016, SourceLair Private Company (https://www.sourcelair.com)
 2012, 2013, Christopher Jeffrey (https://github.com/chjj/)
 2012, 2013, Christopher Jeffrey (MIT License)
License: Expat

Files: addons/xterm-addon-attach/*
Copyright: The xterm.js authors
 2017-2023, The xterm.js authors (https://github.com/xtermjs/xterm.js)
License: Expat

Files: addons/xterm-addon-canvas/*
Copyright: The xterm.js authors
 2017-2023, The xterm.js authors (https://github.com/xtermjs/xterm.js)
License: Expat

Files: addons/xterm-addon-fit/*
Copyright: The xterm.js authors
 2017-2023, The xterm.js authors (https://github.com/xtermjs/xterm.js)
License: Expat

Files: addons/xterm-addon-image/*
Copyright: The xterm.js authors
 2017-2023, The xterm.js authors (https://github.com/xtermjs/xterm.js)
License: Expat

Files: addons/xterm-addon-ligatures/*
Copyright: The xterm.js authors
 2018
License: Expat

Files: addons/xterm-addon-search/*
Copyright: The xterm.js authors
 2017-2023, The xterm.js authors (https://github.com/xtermjs/xterm.js)
License: Expat

Files: addons/xterm-addon-serialize/*
Copyright: The xterm.js authors
License: Expat

Files: addons/xterm-addon-unicode11/*
Copyright: The xterm.js authors
 2017-2023, The xterm.js authors (https://github.com/xtermjs/xterm.js)
License: Expat

Files: addons/xterm-addon-web-links/*
Copyright: The xterm.js authors
 2017-2023, The xterm.js authors (https://github.com/xtermjs/xterm.js)
License: Expat

Files: addons/xterm-addon-webgl/*
Copyright: The xterm.js authors
 2017-2023, The xterm.js authors (https://github.com/xtermjs/xterm.js)
License: Expat

Files: css/*
Copyright: 2014, The xterm.js authors.
 2012, 2013, Christopher Jeffrey (MIT License)
License: Expat

Files: debian/*
Copyright: 2017, Ximin Luo <infinity0@debian.org>
 2017, Ghislain Antony Vaillant <ghisvail@gmail.com>
License: Expat

Files: debian/build_modules/@types/utf8/*
Copyright: Microsoft Corporation.
 2017, Ximin Luo <infinity0@debian.org>
 2017, Ghislain Antony Vaillant <ghisvail@gmail.com>
License: Expat

Files: debian/build_modules/source-map-loader/*
Copyright: Tobias Koppers @sokra
 JS Foundation and other contributors
 2017, Ximin Luo <infinity0@debian.org>
 2017, Ghislain Antony Vaillant <ghisvail@gmail.com>
License: Expat

Files: debian/build_modules/utf8/*
Copyright: Mathias Bynens <https://mathiasbynens.be/>
License: Expat

Files: addons/xterm-addon-attach/typings/* addons/xterm-addon-canvas/src/* addons/xterm-addon-canvas/typings/* addons/xterm-addon-fit/typings/* addons/xterm-addon-image/src/* addons/xterm-addon-image/typings/* addons/xterm-addon-ligatures/bin/* addons/xterm-addon-ligatures/src/* addons/xterm-addon-ligatures/typings/* addons/xterm-addon-search/typings/* addons/xterm-addon-serialize/src/* addons/xterm-addon-serialize/typings/* addons/xterm-addon-unicode11/src/* addons/xterm-addon-unicode11/typings/* addons/xterm-addon-web-links/src/* addons/xterm-addon-web-links/typings/* addons/xterm-addon-webgl/src/* addons/xterm-addon-webgl/typings/* bin/* src/* test/*
Copyright: 2017-2019 The xterm.js authors <https://github.com/xtermjs/xterm.js>
 2014-2016 SourceLair Private Company <https://www.sourcelair.com>
 2012-2013 Christopher Jeffrey <https://github.com/chjj>
License: Expat

Files: debian/build_modules/utf8/utf8.js
Copyright: 2017, Ximin Luo <infinity0@debian.org>
 2017, Ghislain Antony Vaillant <ghisvail@gmail.com>
License: Expat

Files: src/browser/decorations/*
Copyright: Microsoft Corporation.
License: Expat

Files: src/common/services/InstantiationService.ts
 src/common/services/ServiceRegistry.ts
Copyright: 2019, The xterm.js authors.
License: Expat

Files: xterm-wasm-parts/*
Copyright: Joerg Breitbart <j.breitbart@netzkolchose.de>
License: Expat

Files: addons/xterm-addon-attach/src/AttachAddon.ts addons/xterm-addon-attach/test/AttachAddon.api.ts addons/xterm-addon-attach/webpack.config.js addons/xterm-addon-canvas/webpack.config.js addons/xterm-addon-fit/src/FitAddon.ts addons/xterm-addon-fit/test/FitAddon.api.ts addons/xterm-addon-fit/webpack.config.js addons/xterm-addon-image/test/ImageAddon.api.ts addons/xterm-addon-image/webpack.config.js addons/xterm-addon-ligatures/webpack.config.js addons/xterm-addon-search/src/SearchAddon.ts addons/xterm-addon-search/test/SearchAddon.api.ts addons/xterm-addon-search/webpack.config.js addons/xterm-addon-serialize/benchmark/SerializeAddon.benchmark.ts addons/xterm-addon-serialize/test/SerializeAddon.api.ts addons/xterm-addon-serialize/webpack.config.js addons/xterm-addon-unicode11/test/Unicode11Addon.api.ts addons/xterm-addon-unicode11/webpack.config.js addons/xterm-addon-web-links/test/WebLinksAddon.api.ts addons/xterm-addon-web-links/webpack.config.js addons/xterm-addon-webgl/test/WebglRenderer.api.ts addons/xterm-addon-webgl/webpack.config.js demo/client.ts demo/start.js demo/webpack.config.js src/browser/Terminal.ts src/browser/decorations/ColorZoneStore.test.ts src/common/CoreTerminal.ts src/common/InputHandler.ts src/common/input/Keyboard.ts src/headless/Terminal.ts webpack.config.headless.js webpack.config.js
Copyright: 2017-2019 The xterm.js authors <https://github.com/xtermjs/xterm.js>
 2014-2016 SourceLair Private Company <https://www.sourcelair.com>
 2012-2013 Christopher Jeffrey <https://github.com/chjj>
License: Expat

Files: xterm-wasm-parts/src/base64/Base64Decoder.wasm.ts xterm-wasm-parts/src/base64/Base64Encoder.wasm.ts
Copyright: 2023, The xterm.js authors <https://github.com/xtermjs/xterm.js>
License: Expat

Files: xterm-wasm-parts/src/base64/Base64Decoder.wasm.ts xterm-wasm-parts/src/base64/Base64Encoder.wasm.ts xterm-wasm-parts/src/index.ts
Copyright: Joerg Breitbart <j.breitbart@netzkolchose.de>
License: Expat
